﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyLib.Data.Migrations
{
    public partial class ForumUpd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpd",
                table: "Forum",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastUpd",
                table: "Forum");
        }
    }
}
