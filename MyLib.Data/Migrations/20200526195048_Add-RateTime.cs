﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyLib.Data.Migrations
{
    public partial class AddRateTime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "RateTime",
                table: "Ratings",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RateTime",
                table: "Ratings");
        }
    }
}
