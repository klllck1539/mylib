﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyLib.Data.Migrations
{
    public partial class editbookentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Content",
                table: "Books",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PortedText",
                table: "Books",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProcessedText",
                table: "Books",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Content",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "PortedText",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "ProcessedText",
                table: "Books");
        }
    }
}
