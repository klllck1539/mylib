﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyLib.Data.Models
{
    public class Book
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Author { get; set; }

        public string Description { get; set; }

        [Required]
        public int Year { get; set; }

        [Required]
        public DateTime PublicationDate { get; set; }

        public string ImageUrl { get; set; }

        [Required]
        public double Rating { get; set; }

        [Required]
        public string ISBN { get; set; }

        [Required]
        public string Genre { get; set; }

        [Required]
        public string Status { get; set; }

        [Required]
        public string Content { get; set; }

        public string ProcessedText { get; set; }

        public string PortedText { get; set; }

        //FK's
        public virtual User User { get; set; }
        public IEnumerable<BookTag> BookTags { get; set; }
        public virtual IEnumerable<Comment> Comments { get; set; }
        public virtual IEnumerable<Rating> Ratings { get; set; }
    }
}
