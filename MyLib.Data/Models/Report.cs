﻿using System;

namespace MyLib.Data.Models
{
    public class Report
    {
        //PK
        public int Id { get; set; }

        //Atributes
        public int Reason { get; set; }
        public string Description { get; set; }
        public DateTime ReportDate { get; set; }
        public bool IsRead { get; set; } //уведомление админу/модеру

        //FKs
        public Comment Comment { get; set; } 
        public User UserReporter { get; set; }
    }
}
