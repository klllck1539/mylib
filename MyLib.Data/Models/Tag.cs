﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MyLib.Data.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public TagType Type { get; set; }

        //Fk's
        public IEnumerable<BookTag> BookTags { get; set; }
    }

    public enum TagType
    {
        [Display(Name = "Author")]
        Author,

        [Display(Name = "Genre")]
        Genre,

        [Display(Name = "All")]
        All
    }
}
