﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyLib.Data.Models
{
    public class Forum
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public string Content { get; set; }

        [Required]
        public DateTime Created { get; set; }

        public DateTime LastUpd { get; set; }
        public int LikeCount { get; set; }
        public int DislikeCount { get; set; }

        //FK's
        public virtual User User { get; set; }
        public virtual IEnumerable<Comment> Comments { get; set; }
        public virtual IEnumerable<Rating> Ratings { get; set; }
    }
}
