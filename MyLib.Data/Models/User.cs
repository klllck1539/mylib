﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyLib.Data.Models
{
    public class User : IdentityUser
    {
        [Required]
        public string FullName { get; set; }

        [Required]
        public int Age { get; set; }

        [Required]
        public double Rating { get; set; }

        [Required]
        public string ProfileImageUrl { get; set; }

        public bool IsDeleted { get; set; }


        //FK's
        public virtual IEnumerable<Book> Books { get; set; }
        public virtual IEnumerable<Forum> Posts { get; set; }
        public virtual IEnumerable<Comment> Comments { get; set; }
        public virtual IEnumerable<Rating> Ratings { get; set; }
        public virtual IEnumerable<Report> Reports { get; set; }
    }
}
