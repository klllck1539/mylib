﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyLib.Data.Models
{
    public class Rating
    {
        public int Id { get; set; }

        public int BookRate { get; set; }
        public int PostRate { get; set; }
        public int CommentRate { get; set; }

        public virtual Forum Post { get; set; }
        public virtual Comment Comment { get; set; }
        public virtual Book Book { get; set; }
        public virtual User User { get; set; }

    }
}
