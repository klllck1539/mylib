﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyLib.Data.Models
{
    public class Comment
    {
        public int Id { get; set; }

        public string Content { get; set; }

        [Required]
        public DateTime Created { get; set; }
        public int LikeCount { get; set; }
        public int DislikeCount { get; set; }

        //FK's
        public virtual User User { get; set; }
        public virtual Forum Forum { get; set; }
        public virtual Book Book { get; set; }
        public virtual IEnumerable<Rating> Ratings { get; set; }
        public virtual IEnumerable<Report> Reports { get; set; }
    }
}
