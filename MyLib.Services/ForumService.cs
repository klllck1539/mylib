﻿using Microsoft.EntityFrameworkCore;
using MyLib.Data;
using MyLib.Data.Models;
using MyLib.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLib.Services
{
    public class ForumService : IForum
    {
        private readonly AppDbContext _context;

        public ForumService(AppDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Forum> GetAll()
        {
            return _context.Forum
                .Include(f => f.Comments).ThenInclude(c => c.User)
                .Include(f => f.User);
        }

        public Forum GetById(int id)
        {
            return GetAll().FirstOrDefault(f => f.Id == id);
        }

        public IEnumerable<Forum> GetFilteredPosts(string searchQuery)
        {
            if (!string.IsNullOrEmpty(searchQuery))
            {
                var query = searchQuery.ToLower();

                return GetAll()
                    .Where(book =>
                        book.Title.ToLower().Contains(query)
                     || book.Content.ToLower().Contains(query));
            }
            else return GetAll();
        }

        public IEnumerable<Comment> GetFilteredComments(int forumId, string searchQuery)
        {
            var forum = GetById(forumId);

            if (!string.IsNullOrEmpty(searchQuery))
            {
                var query = searchQuery.ToLower();

                return forum.Comments
                    .Where(c => c.Content.ToLower().Contains(query));
            }

            else return forum.Comments;
        }

        public bool HasRecentComments(int id)
        {
            const int hoursAgo = 24;
            var lastDay = DateTime.Now.AddHours(-hoursAgo);

            return GetById(id).Comments.Any(post => post.Created > lastDay);
        }

        public IEnumerable<User> GetActiveUsers(int id)
        {
            var comments = GetById(id).Comments;

            if (comments != null || !comments.Any())
            {
                var users = comments.Select(post => post.User);

                return users.Distinct();
            }

            return new List<User>();
        }

        public async Task Create(Forum forum)
        {
            _context.Add(forum);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(int forumId)
        {
            var forum = GetById(forumId);
            _context.Remove(forum);
            await _context.SaveChangesAsync();
        }

        public async Task Update(Forum forum)
        {
            _context.Forum.Update(forum);
            await _context.SaveChangesAsync();
        }
    }
}
