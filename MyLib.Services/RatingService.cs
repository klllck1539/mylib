﻿using Microsoft.EntityFrameworkCore;
using MyLib.Data;
using MyLib.Data.Models;
using MyLib.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLib
{
    public class RatingService : IRating
    {
        private readonly AppDbContext _context;

        public RatingService(AppDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Rating> GetAll()
        {
            return _context.Ratings.Include(r => r.Book)
                .Include(r => r.Post).ThenInclude(p => p.Comments)
                .Include(r => r.User);
        }

        public Rating GetById(int id)
        {
            return GetAll().FirstOrDefault(r => r.Id == id);
        }

        public Rating GetByUserId(string id)
        {
            return GetAll().FirstOrDefault(r => r.User.Id == id);
        }

        public async Task Add(Rating newRate)
        {
            _context.Ratings.Add(newRate);
            await _context.SaveChangesAsync();
        }

        public double CalculateBookRating(int bookId)
        {
            double result = 0.0;
            var books = _context.Ratings.Where(r => r.Book.Id == bookId).ToList();

            foreach (var book in books)
            {
                result += book.BookRate;
            }

            return Math.Round(result / books.Count(), 2);
        }
        
        public bool IsBookRated(int bookId, string userId)
        {
            return _context.Ratings.Any(r => r.Book.Id == bookId && r.User.Id == userId);
        }

        public async Task Update(Rating rating)
        {
            _context.Ratings.Update(rating);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Rating rating)
        {
            _context.Ratings.Remove(rating);
            await _context.SaveChangesAsync();
        }
    }
}
