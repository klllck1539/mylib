﻿using Microsoft.EntityFrameworkCore;
using MyLib.Data;
using MyLib.Data.Models;
using MyLib.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLib.Services
{
    public class UserService : IUser
    {
        private readonly AppDbContext _context;

        public UserService(AppDbContext context)
        {
            _context = context;
        }

        public IEnumerable<User> GetAll()
        {
            return _context.User
                .Include(u => u.Books)
                .Include(u => u.Comments)
                .Include(u => u.Posts).ThenInclude(p => p.Comments);
        }

        public User GetById(string id)
        {
            return GetAll().FirstOrDefault(u => u.Id == id);
        }

        public async Task Delete(string id)
        {
            var user = GetById(id);
            _context.User.Remove(user);
            await _context.SaveChangesAsync();
        }

        public async Task Edit(User user)
        {
            _context.User.Update(user);
            await _context.SaveChangesAsync();
        }

        public Task UpdateUserRating(string id, Type type)
        {
            throw new NotImplementedException();
        }
    }
}
