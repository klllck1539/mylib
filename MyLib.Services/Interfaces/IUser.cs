﻿using MyLib.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyLib.Services.Interfaces
{
    public interface IUser
    {
        IEnumerable<User> GetAll();
        User GetById(string id);
        Task Delete(string id);
        Task Edit(User user);
        Task UpdateUserRating(string id, Type type);
    }
}
