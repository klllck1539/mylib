﻿using MyLib.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyLib.Services.Interfaces
{
    public interface ITag
    {
        Task Add(Tag newBook);
        Task Delete(int id);
        Task Edit(Tag book);

        Task AddTagToBook(Book book, Tag tag);
        Task RemoveTagFromBook(Book book, Tag tag);

        Tag GetById(int id);
        IEnumerable<Tag> GetAll();
        IEnumerable<Tag> GetFilteredTags(string searchQuery, TagType tagType);
    }
}
