﻿using MyLib.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyLib.Services.Interfaces
{
    public interface IComment
    {
        Comment GetById(int id);
        IEnumerable<Comment> GetAll();
        IEnumerable<Comment> GetFilteredComments(string searchQuery);
        IEnumerable<Comment> GetCommentsByForum(int id);
        IEnumerable<Comment> GetLatestComments(int n);

        Task Create(Comment comment);
        Task Delete(int id);
        Task Update(Comment comment);

    }
}
