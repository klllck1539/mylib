﻿using MyLib.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyLib.Services.Interfaces
{
    public interface IBook 
    {
        Task Add(Book newBook);
        Task Delete(int id);
        Task DeleteAll();
        Task Edit(Book book);
        Task AddReview(Comment comment);

        Book GetById(int id);
        IEnumerable<Book> GetAll();
        IEnumerable<Book> GetFilteredBooks(string searchQuery);
        IEnumerable<Book> GetLatestBooks(int n);
        IEnumerable<Book> GetBookWithTags(IEnumerable<Book> books, string searchQuery);
    }
}
