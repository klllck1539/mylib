﻿using MyLib.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyLib.Services.Interfaces
{
    public interface IRating
    {
        IEnumerable<Rating> GetAll();
        Rating GetById(int id);
        Rating GetByUserId(string id);
        Task Add(Rating newRate);
        Task Update(Rating rating);
        Task Delete(Rating rating);
        double CalculateBookRating(int bookId);
        bool IsBookRated(int bookId, string userId);
    }
}
