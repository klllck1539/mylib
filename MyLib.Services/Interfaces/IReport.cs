﻿using MyLib.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyLib.Services.Interfaces
{
    public interface IReport
    {
        Task Add(Report newReport);
        Task Delete(int id);
        Task DeleteAll();
        int CountUnread();
        Task CheckReport(Report newReport);

        Report GetById(int id);
        IEnumerable<Report> GetAll();
    }
}
