﻿using MyLib.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyLib.Services.Interfaces
{
    public interface IForum
    {
        Forum GetById(int id);
        IEnumerable<Forum> GetAll();
        IEnumerable<Forum> GetFilteredPosts(string searchQuery);
        IEnumerable<Comment> GetFilteredComments(int forumId, string searchQuery);
        IEnumerable<User> GetActiveUsers(int id);
        bool HasRecentComments(int id);

        Task Create(Forum forum);
        Task Delete(int forumId);
        Task Update(Forum forum);
    }
}
