﻿using Microsoft.EntityFrameworkCore;
using MyLib.Data;
using MyLib.Data.Models;
using MyLib.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLib.Services
{
    public class TagService : ITag
    {
        private readonly AppDbContext _context;

        public TagService(AppDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Tag> GetAll()
        {
            return _context.Tags
                .Include(b => b.BookTags);
        }

        public Tag GetById(int id)
        {
            return GetAll().FirstOrDefault(b => b.Id == id);
        }

        public IEnumerable<Tag> GetFilteredTags(string searchQuery, TagType tagType = TagType.All)
        {
            var query = searchQuery.ToLower();
            IEnumerable<Tag> result;

            if (!string.IsNullOrEmpty(query))
                result = GetAll().Where(tag => tag.Name.Contains(query));
            else
                result = GetAll();

            if (tagType != TagType.All)
                result = result.Where(tag => tag.Type == tagType);

            return result;
        }

        public async Task Add(Tag newTag)
        {
            _context.Tags.Add(newTag);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var tag = GetById(id);
            _context.Tags.Remove(tag);
            await _context.SaveChangesAsync();
        }

        public async Task Edit(Tag tag)
        {
            _context.Tags.Update(tag);
            await _context.SaveChangesAsync();
        }

        public async Task AddTagToBook(Book book, Tag tag)
        {
            if (!book.BookTags.Any(bt => bt.Tag.Id == tag.Id))
            {
                var bookTag = new BookTag
                {
                    Book = book,
                    Tag = tag
                };
                _context.BookTags.Add(bookTag);
                await _context.SaveChangesAsync();
            }
        }

        public async Task RemoveTagFromBook(Book book, Tag tag)
        {
            if (book.BookTags.Any(bt => bt.Tag.Id == tag.Id))
            {
                var bookTag = book.BookTags.FirstOrDefault(bt => bt.Tag == tag);
                _context.BookTags.Remove(bookTag);
                await _context.SaveChangesAsync();
            }
        }
    }
}
