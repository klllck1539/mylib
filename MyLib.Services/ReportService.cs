﻿using Microsoft.EntityFrameworkCore;
using MyLib.Data;
using MyLib.Data.Models;
using MyLib.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLib.Services
{
    public class ReportService : IReport
    {
        private readonly AppDbContext _context;

        public ReportService(AppDbContext context)
        {
            _context = context;
        }
        public async Task Add(Report newReport)
        {
            _context.Reports.Add(newReport);
            await _context.SaveChangesAsync();
        }

        public async Task CheckReport(Report newReport)
        {
            newReport.IsRead = true;
            _context.Reports.Update(newReport);
            await _context.SaveChangesAsync();
        }

        public int CountUnread() 
        { 
            return GetAll().Where(x => !x.IsRead).ToList().Count;
        }

        public async Task Delete(int id)
        {
            var report = GetById(id);
            _context.Reports.Remove(report);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAll()
        {
            var reports = GetAll();
            foreach (var report in reports)
                _context.Reports.Remove(report);
            await _context.SaveChangesAsync();
        }

        public IEnumerable<Report> GetAll() 
        { 
            return _context.Reports.Include(r => r.UserReporter)
                .Include(r => r.Comment).ThenInclude(r => r.User); 
        }

        public Report GetById(int id)
        {
            return GetAll().FirstOrDefault(r => r.Id == id);
        }
    }
}
