﻿using MyLib.Data.Models;
using MyLib.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyLib.Data;
using System.Linq;

namespace MyLib.Services
{
    public class CommentService : IComment
    {
        private readonly AppDbContext _context;

        public CommentService(AppDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Comment> GetAll()
        {
            return _context.Comments.Include(c => c.Book)
                .Include(c => c.User)
                .Include(c => c.Forum);
        }

        public Comment GetById(int id)
        {
            return GetAll().FirstOrDefault(c => c.Id == id);
        }

        public IEnumerable<Comment> GetFilteredComments(string searchQuery)
        {
            if (!string.IsNullOrEmpty(searchQuery))
            {
                var query = searchQuery.ToLower();

                return GetAll()
                    .Where(c => c.Content.ToLower().Contains(query));
            }

            return GetAll();
        }

        public IEnumerable<Comment> GetLatestComments(int n)
        {
            return GetAll().OrderByDescending(c => c.Created).Take(n);
        }

        public IEnumerable<Comment> GetCommentsByForum(int id)
        {
            return _context.Forum.Where(forum => forum.Id == id)
                .First().Comments;
        }

        public async Task Create(Comment comment)
        {
            _context.Add(comment);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var forum = GetById(id);
            _context.Remove(forum);
            await _context.SaveChangesAsync();
        }

        public async Task Update(Comment comment)
        {
            _context.Update(comment);
            await _context.SaveChangesAsync();
        }
    }
}
