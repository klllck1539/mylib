﻿using Microsoft.EntityFrameworkCore;
using MyLib.Data;
using MyLib.Data.Models;
using MyLib.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLib.Services
{
    public class BookService : IBook
    {
        private readonly AppDbContext _context;

        public BookService(AppDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Book> GetAll()
        {
            return _context.Books
                .Include(b => b.User)
                .Include(b => b.Comments)
                .Include(b => b.Ratings)
                .Include(b => b.BookTags).ThenInclude(bt => bt.Tag);
        }

        public Book GetById(int id)
        {
            return GetAll().FirstOrDefault(b => b.Id == id);
        }

        public IEnumerable<Book> GetLatestBooks(int n)
        {
            return GetAll()
                .OrderByDescending(b => b.PublicationDate).Take(n);
        }

        public IEnumerable<Book> GetFilteredBooks(string searchQuery)
        {
            if (!string.IsNullOrEmpty(searchQuery))
            {
                var query = searchQuery.ToLower();

                return GetAll()
                    .Where(book =>
                        book.Title.ToLower().Contains(query)
                     || book.Author.ToLower().Contains(query)
                     || book.Genre.ToLower().Contains(query));
            }
            else return GetAll();
        }

        public IEnumerable<Book> GetBookWithTags(IEnumerable<Book> books, string searchQuery)
        {
            if (string.IsNullOrEmpty(searchQuery))
            {
                return books;
            }
            var tags = searchQuery.ToLower().Split(",").Select(str => str.Trim()).ToList();

            return books.Where(book => IsTagContains(book, tags)).ToList();
        }

        private bool IsTagContains(Book book, List<string> tagsName)
        {
            var bookTags = book.BookTags.Select(bt => bt.Tag.Name.ToLower());
            var temp = tagsName.Intersect(bookTags).ToList();

            return tagsName.Count != temp.Count ? false : true;
        }

        public async Task Add(Book newBook)
        {
            _context.Books.Add(newBook);
            //begin adding
            /*var post = new Forum
            {
                Id = 0,
                Title = $"{newBook.Author}, \"{newBook.Title}\"",
                Content = "Данная тема была создана автоматически при загрузке книги",
                Created = DateTime.Now,
                User = newBook.User,
                LastUpd = DateTime.Now
            };
            _context.Forum.Add(post);*/

            await _context.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var book = GetById(id);

            var rates = _context.Ratings.Where(r => r.Book.Id == book.Id);
            foreach (var rate in rates)
                _context.Ratings.Remove(rate);

            _context.Books.Remove(book);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAll()
        {
            var books = GetAll();

            foreach (var book in books)
            {
                var rates = _context.Ratings.Where(r => r.Book.Id == book.Id);
                foreach (var rate in rates)
                {
                    _context.Ratings.Remove(rate);
                }
                _context.Books.Remove(book);
            }

            await _context.SaveChangesAsync();
        }

        public async Task Edit(Book book)
        {
            _context.Books.Update(book);
            await _context.SaveChangesAsync();
        }

        public async Task AddReview(Comment comment)
        {
            _context.Comments.Add(comment);
            await _context.SaveChangesAsync();
        }
    }
}
