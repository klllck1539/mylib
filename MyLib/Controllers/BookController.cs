﻿using k_clustering.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyLib.Data.Models;
using MyLib.Services.Interfaces;
using MyLib.ViewModels;
using MyLib.ViewModels.Book;
using MyLib.ViewModels.Tag;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MyLib.Controllers
{
    public class BookController : Controller
    {
        private readonly IBook _bookService;
        private readonly IRating _ratingService;
        private readonly UserManager<User> _userManager;
        private readonly IWebHostEnvironment _hostingEnvironment;

        private readonly Regex _regex = new Regex("([ \\t{}()\",:;. \n])");

        public BookController(IBook bookService,
            IRating ratingService,
            UserManager<User> userManager,
            IWebHostEnvironment hostingEnvironment)
        {
            _bookService = bookService;
            _ratingService = ratingService;
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        public IActionResult Search(string searchQuery)
        {
            return RedirectToAction("Index", new { searchQuery });
        }

        public IActionResult Index(string searchQuery, int page = 1)
        {
            int pageSize = 3;
            var books = _bookService.GetFilteredBooks(searchQuery).OrderByDescending(x => x.PublicationDate).ToList();
            var noResults = !string.IsNullOrEmpty(searchQuery) && !books.Any();

            var booksOnPage = books
                .Select(book => new BookViewModel
                {
                    BookId = book.Id,
                    Title = book.Title,
                    ImageUrl = book.ImageUrl,
                    Author = book.Author,
                    Genre = book.Genre,
                    Rating = book.Rating,
                    PublicationDate = book.PublicationDate,
                    UserId = book.User.Id,
                    Year = book.Year,
                    Description = book.Description,
                    Tags = book.BookTags.Where(bt => bt.BookId == book.Id).ToList().Select(bt => new TagViewModel
                    {
                        Id = bt.Tag.Id,
                        Name = bt.Tag.Name,
                        Type = bt.Tag.Type
                    })
                }).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            PageViewModel pageViewModel = new PageViewModel(books.Count(), page, pageSize, searchQuery);

            var model = new BookIndexModel()
            {
                Books = booksOnPage,
                PageViewModel = pageViewModel,
                SearchQuery = searchQuery
            };

            List<int> rates = new List<int>();
            foreach (var book in booksOnPage)
                rates.Add(_ratingService.GetAll().Where(x => x.Book.Id == book.BookId).ToList().Count);

            ViewBag.Rates = rates;

            return View(model);
        }

        public IActionResult Detail(int id, int page = 1)
        {            
            var book = _bookService.GetById(id);
            var genres = book.Genre.ToLower().Split(new char[] { ',', ' ' }).ToList();
            int pageSize = genres.Contains("поэзия") || genres.Contains("пьеса") ? 50 : 25;

            var rates = _ratingService.GetAll().Where(x => x.Book.Id == id).ToList().Count;
            ViewBag.Rates = rates;

            var bookTags = book.BookTags.Where(bt => bt.BookId == id).ToList();

            var tags = bookTags
                .Select(bt => new TagViewModel
                {
                    Id = bt.Tag.Id,
                    Name = bt.Tag.Name,
                    Type = bt.Tag.Type
                });

            var sentences = book.Content.Split(new char[] { '\n' }).ToList();
            var chapters = book.Content.Split(new char[] { '|' }).ToList();

            List<string> headings = new List<string>();
            foreach (var chapter in chapters)
                headings.Add(chapter.Split(new char[] { '\n' }).FirstOrDefault());

            for (int i = 0; i < sentences.Count - 1; i++)
            {
                if (sentences[i] == "\r" && sentences[i + 1] == "\r")
                {
                    sentences.RemoveAt(i + 1);
                    i--;
                }
            }

            var paginText = sentences.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            PageViewModel pageViewModel = new PageViewModel(sentences.Count, page, pageSize, null);

            ViewBag.Heads = headings.Count;

            var model = new BookViewModel
            {
                BookId = id,
                Title = book.Title,
                Content = book.Content,
                PublicationDate = book.PublicationDate,
                Year = book.Year,
                Status = book.Status,
                ImageUrl = book.ImageUrl,
                Description = book.Description,
                Author = book.Author,
                Genre = book.Genre,
                Isbn = book.ISBN,
                Rating = book.Rating,
                UserId = book.User.Id,
                Tags = tags,
                Pages = pageViewModel,
                Sentences = headings.Count > 1 ? headings : paginText
            };

            ViewBag.IsVoted = _ratingService.IsBookRated(book.Id, _userManager.GetUserId(User));

            return View(model);
        }

        public IActionResult Read(int id, int chapter, int page = 1)
        {
            var book = _bookService.GetById(id);
            var genres = book.Genre.ToLower().Split(new char[] { ',', ' ' }).ToList();
            int pageSize = genres.Contains("поэзия") || genres.Contains("пьеса") ? 50 : 25;

            var rates = _ratingService.GetAll().Where(x => x.Book.Id == id).ToList().Count;
            ViewBag.Rates = rates;

            var bookTags = book.BookTags.Where(bt => bt.BookId == id).ToList();

            var tags = bookTags
                .Select(bt => new TagViewModel
                {
                    Id = bt.Tag.Id,
                    Name = bt.Tag.Name,
                    Type = bt.Tag.Type
                });

            //begin хрень
            var chapters = book.Content.Split(new char[] { '|' }).ToList();

            var content = chapters[chapter];
            var sentences = content.Split(new char[] { '\n' }).ToList();

            for (int i = 0; i < sentences.Count - 1; i++)
            {
                if (sentences[i] == "\r" && sentences[i + 1] == "\r")
                {
                    sentences.RemoveAt(i + 1);
                    i--;
                }
            }

            var paginText = sentences.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            PageViewModel pageViewModel = new PageViewModel(sentences.Count, chapter, page, pageSize);

            var model = new BookViewModel
            {
                BookId = id,
                Title = book.Title,
                Content = book.Content,
                PublicationDate = book.PublicationDate,
                Year = book.Year,
                Status = book.Status,
                ImageUrl = book.ImageUrl,
                Description = book.Description,
                Author = book.Author,
                Genre = book.Genre,
                Isbn = book.ISBN,
                Rating = book.Rating,
                UserId = book.User.Id,
                Tags = tags,
                Pages = pageViewModel,
                Sentences = paginText
            };

            ViewBag.IsVoted = _ratingService.IsBookRated(book.Id, _userManager.GetUserId(User));

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> RateBook(BookViewModel model)
        {
            var book = _bookService.GetById(model.BookId);
            var userId = _userManager.GetUserId(User);
            if (userId == null)
            {
                {
                    ViewBag.ErrorMessage = $"Пользователь с Id : {userId} не может быть найден";
                    return NotFound();
                }
            }
            var user = _userManager.FindByIdAsync(userId).Result;

            if (_ratingService.IsBookRated(book.Id, userId))
            {
                return RedirectToAction("Index");
            }

            var newBookRate = new Rating
            {
                BookRate = model.RateValue,
                Book = book,
                User = user
            };

            await _ratingService.Add(newBookRate);

            book.Rating = _ratingService.CalculateBookRating(book.Id);

            await _bookService.Edit(book);

            return RedirectToAction("Detail", new { id = model.BookId });
        }

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddBook(BookViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = _userManager.GetUserId(User);
                if (userId == null)
                {
                    ViewBag.ErrorMessage = $"Пользователь с Id : {userId} не может быть найден";
                    return NotFound();
                }

                var user = _userManager.FindByIdAsync(userId).Result;

                string imgUrl = UploadBookImage(model);
                var bookContent = ExtractTextFromFile(model);
                var transformText = TransformText(bookContent);

                var newBook = new Book
                {
                    Title = model.Title,
                    Author = model.Author,
                    Description = model.Description,
                    Genre = model.Genre,
                    Year = model.Year,
                    ImageUrl = imgUrl,
                    PublicationDate = DateTime.Now,
                    Status = "Avaliable",
                    ISBN = GetIsbn(),
                    User = user,
                    Content = bookContent,
                    ProcessedText = string.Join(",", transformText.Item1.ToArray()),
                    PortedText = string.Join(",", transformText.Item2.ToArray())
                };

                await _bookService.Add(newBook);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            await _bookService.Delete(id);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var book = _bookService.GetById(id);

            if (book == null)
            {
                ViewBag.ErrorMassage = $"Не найдена книга с Id = {id}";
                return NotFound();
            }

            var bookEditView = new BookViewModel
            {
                BookId = book.Id,
                Title = book.Title,
                Author = book.Author,
                Description = book.Description,
                Genre = book.Genre,
                Year = book.Year,
                Isbn = book.ISBN,
                PublicationDate = book.PublicationDate,
                Status = book.Status,
                ExistingImageUrl = book.ImageUrl
            };

            return View(bookEditView);
        }

        [HttpPost, ActionName("Edit")]
        public async Task<IActionResult> EditBook(BookViewModel model)
        {
            if (ModelState.IsValid)
            {
                var book = _bookService.GetById(model.BookId);

                if (book == null)
                {
                    ViewBag.ErrorMassage = $"Не найдена книга с Id = {model.BookId}";
                    return NotFound();
                }
                else
                {
                    book.Title = model.Title;
                    book.Author = model.Author;
                    book.Description = model.Description;
                    book.Genre = model.Genre;
                    book.Year = model.Year;
                    book.ImageUrl = UploadBookImage(model) ?? book.ImageUrl;
                }

                await _bookService.Edit(book);

                return RedirectToAction("Index", "Book");
            }

            return View(model);
        }
        private string UploadBookImage(BookViewModel model)
        {
            if (model.Image != null)
            {
                var uploadsFolder = _hostingEnvironment.WebRootPath + "\\images\\books";
                var fileUrl = model.Image.FileName;
                var filePath = Path.Combine(uploadsFolder, fileUrl);
                var stream = new FileStream(filePath, FileMode.Create);
                model.Image.CopyTo(stream);
                stream.Close();

                return @"/images/books/" + fileUrl;
            }

            return "/images/books/noimage.png";
        }

        private string ExtractTextFromFile(BookViewModel model)
        {
            if (model.FileContent != null)
            {
                var uploadsFolder = _hostingEnvironment.WebRootPath + "\\books\\textfiles";
                var fileUrl = model.FileContent.FileName;
                var filePath = Path.Combine(uploadsFolder, fileUrl);
                var stream = new FileStream(filePath, FileMode.Create);
                model.FileContent.CopyTo(stream);
                stream.Close();

                var text = System.IO.File.ReadAllText(filePath, System.Text.Encoding.GetEncoding("Windows-1251"));

                return text;
            }

            return "Содержание этой книги не доступно";
        }

        private (List<string>, List<string>) TransformText(string text)
        {
            var processedText = new List<string>();
            var porterText = new List<string>();

            foreach (var term in _regex.Split(text))
            {
                var temp1 = Regex.Replace(term.ToLower(), @"[^а-я]", string.Empty);

                var temp2 = Porter.TransformWord(temp1);

                if (temp1 != "")
                {
                    processedText.Add(temp1);
                    porterText.Add(temp2);
                }
            }

            return (processedText, porterText);
        }

        public string GetIsbn()
        {
            var random = new Random();
            string str = string.Empty;
            for (int i = 0; i < 10; i++)
                str = string.Concat(str, random.Next(10).ToString());
            return str;
        }
    }
}
