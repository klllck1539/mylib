﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using k_clustering.k_means;
using k_clustering.k_means.Objects;
using k_clustering.TextClustering;
using k_clustering.TextClustering.SimilarityMetrics;
using k_clustering.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyLib.Clustering.TextClustering;
using MyLib.Clustering.TextClustering.MLObjects;
using MyLib.Data.Models;
using MyLib.Services.Interfaces;
using MyLib.ViewModels.Clustering;

namespace MyLib.Controllers
{
    public class ClusterController : Controller
    {
        private readonly IBook _bookService;
        private readonly ITag _tagService;
        private readonly IRating _ratingService;
        private readonly UserManager<User> _userManager;
        private readonly IWebHostEnvironment _hostingEnvironment;

        private readonly Regex _regex = new Regex("([ \\t{}()\",:;. \n])");

        public ClusterController(IBook bookService,
            ITag tagService,
            IRating ratingService,
            UserManager<User> userManager,
            IWebHostEnvironment hostingEnvironment)
        {
            _bookService = bookService;
            _tagService = tagService;
            _ratingService = ratingService;
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        public IActionResult Search(string searchQueryName, string searchQueryTag)
        {
            return RedirectToAction("Index", new { searchQueryName, searchQueryTag });
        }

        public IActionResult Index(string searchQueryName, string searchQueryTag)
        {
            var books = _bookService.GetFilteredBooks(searchQueryName).ToList();
            var booksWithTags = _bookService.GetBookWithTags(books, searchQueryTag).ToList();

            var bookModel = booksWithTags
                .Select(book => new ClusteringBookViewModel
                {
                    BookId = book.Id,
                    Title = book.Title,
                    ImageUrl = book.ImageUrl,
                    Content = book.Content,
                    ProcessedText = book.ProcessedText,
                    PortedText = book.PortedText,
                    Tags = book.BookTags,
                    Author = book.Author
                }).ToList();

            var tags = _tagService.GetAll().ToList();

            var tagModel = tags
                .Select(tag => new ClusteringTagViewModel
                {
                    Id = tag.Id,
                    Name = tag.Name,
                    Type = tag.Type
                }).ToList();

            var model = new ClusteringIndexViewModel()
            {
                Books = bookModel,
                Tags = tagModel,
                BookChecks = Enumerable.Repeat(true, bookModel.Count).ToList()
            };

            ViewBag.Books = model.Books;
            return View(model);
        }

        [HttpGet]
        public IActionResult ClusteringResults(ClusteringResultsViewModel model)
        {
            return View(model);
        }

        [HttpPost]
        public IActionResult ClusteringResults(ClusteringIndexViewModel model)
        {
            var books = model.BookIds.Select((id, index) =>  (model.BookChecksInt[index] == 1) ? _bookService.GetById(id) : null).Where(elem => elem!=null);

            List<Cluster> clusters;

            if (!model.MLNet)
            {
                DocumentVectorModel documentVectorModel = new DocumentVectorModel(false);
                documentVectorModel.CreateDVCollection(books.Select(book
                    => book.ProcessedText).ToList(), books.Select(book
                    => book.Id).ToArray());

                K_means kMeans = new K_means(new CosineSimilarity());

                if (model.XMeans)
                    clusters = kMeans.XCluster(documentVectorModel.DocumentVectorCollection);
                else
                    clusters = kMeans.Cluster(documentVectorModel.DocumentVectorCollection, model.K);
            }
            else
            {
                var mLBooks = books.Select(book => new MLBook
                {
                    Id = book.Id,
                    Text = Regex.Replace(book.Content.ToLower(), @"[^ а-я]", string.Empty)  
                });


                MLClustering mLClustering = new MLClustering();

                clusters = mLClustering.Cluster(mLBooks, model.K);
            }

            var bookClusters = new List<List<ClusteringBookViewModel>>();

            foreach (var cluster in clusters)
            {
                bookClusters.Add(new List<ClusteringBookViewModel>());

                for (int i = 0; i < cluster.DocumentGroup.Count; i++)
                {
                    Book book = _bookService.GetById(cluster.DocumentGroup[i].Id);

                    var bookModel = new ClusteringBookViewModel()
                    {
                        BookId = book.Id,
                        Author = book.Author,
                        Title = book.Title,
                        ImageUrl = book.ImageUrl,
                        Content = book.Content,
                        ProcessedText = book.ProcessedText,
                        PortedText = book.PortedText,
                        Tags = book.BookTags.OrderBy(tag=>tag.Tag.Type)
                    };

                    bookClusters.Last().Add(bookModel);
                }
            }

            var tagsModel = _tagService.GetAll().Select(tag => new ClusteringTagViewModel
            {
                Id = tag.Id,
                Name = tag.Name,
                Type = tag.Type
            }).ToList();

            var viewModel = new ClusteringResultsViewModel()
            {
                BookClusters = bookClusters,
                Tags = tagsModel,
                BookChecks = Enumerable.Repeat(true, books.Count()).ToList()
            };


            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> AddTags(ClusteringResultsViewModel model)
        {
            var books = model.BookIds.Select((id, index) => (model.BookChecksInt[index] == 1) ? _bookService.GetById(id) : null).Where(book => book != null).ToList();

            await Task.Delay(1000);

            var tags = model.TagIds.Select((id, index) => (model.TagChecksInt[index] == 1) ? _tagService.GetById(id) : null).Where(tag => tag != null).ToList();

            await Task.Delay(1000);

            foreach (var tag in tags)
            {
                foreach (var book in books)
                {
                    await _tagService.AddTagToBook(book, tag);
                }
            }

            await Task.Delay(1000);

            var allBooks = model.BookIds.Select(id => _bookService.GetById(id)).ToList();

            var bookClusters = new List<List<ClusteringBookViewModel>>();


            int temp = model.ClusterIds[0];
            bookClusters.Add(new List<ClusteringBookViewModel>());
            for (int i = 0; i < model.ClusterIds.Count; i++)
            {
                if (temp != model.ClusterIds[i])
                    bookClusters.Add(new List<ClusteringBookViewModel>());

                temp = model.ClusterIds[i];

                Book book = allBooks[i];

                var bookModel = new ClusteringBookViewModel()
                {
                    BookId = book.Id,
                    Author = book.Author,
                    Title = book.Title,
                    ImageUrl = book.ImageUrl,
                    Content = book.Content,
                    ProcessedText = book.ProcessedText,
                    PortedText = book.PortedText,
                    Tags = book.BookTags.OrderBy(tag => tag.Tag.Type)
                };

                bookClusters.Last().Add(bookModel);
            }

            var tagsModel = _tagService.GetAll().Select(tag => new ClusteringTagViewModel
            {
                Id = tag.Id,
                Name = tag.Name,
                Type = tag.Type
            }).ToList();

            var viewModel = new ClusteringResultsViewModel()
            {
                BookClusters = bookClusters,
                Tags = tagsModel,
                BookChecks = Enumerable.Repeat(true, books.Count()).ToList()
            };

            return View("ClusteringResults", viewModel);
        }

        //Legendary
        [HttpPost]
        public async Task<IActionResult> DeleteTags(ClusteringResultsViewModel model)
        {
            var books = model.BookIds.Select((id, index) => (model.BookChecksInt[index] == 1) ? _bookService.GetById(id) : null).Where(book => book != null).ToList();

            await Task.Delay(1000);

            var tags = model.TagIds.Select((id, index) => (model.TagChecksInt[index] == 1) ? _tagService.GetById(id) : null).Where(tag => tag != null).ToList();

            await Task.Delay(1000);

            foreach (var tag in tags)
            {
                foreach (var book in books)
                {
                    await _tagService.RemoveTagFromBook(book, tag);
                }
            }

            await Task.Delay(1000);

            var allBooks = model.BookIds.Select(id => _bookService.GetById(id)).ToList();

            var bookClusters = new List<List<ClusteringBookViewModel>>();

            int temp = model.ClusterIds[0];
            bookClusters.Add(new List<ClusteringBookViewModel>());
            for (int i = 0; i < model.ClusterIds.Count; i++)
            {
                if (temp != model.ClusterIds[i])
                    bookClusters.Add(new List<ClusteringBookViewModel>());

                temp = model.ClusterIds[i];

                Book book = allBooks[i];

                var bookModel = new ClusteringBookViewModel()
                {
                    BookId = book.Id,
                    Author = book.Author,
                    Title = book.Title,
                    ImageUrl = book.ImageUrl,
                    Content = book.Content,
                    ProcessedText = book.ProcessedText,
                    PortedText = book.PortedText,
                    Tags = book.BookTags.OrderBy(tag => tag.Tag.Type)
                };

                bookClusters.Last().Add(bookModel);
            }

            var tagsModel = _tagService.GetAll().Select(tag => new ClusteringTagViewModel
            {
                Id = tag.Id,
                Name = tag.Name,
                Type = tag.Type
            }).ToList();

            var viewModel = new ClusteringResultsViewModel()
            {
                BookClusters = bookClusters,
                Tags = tagsModel,
                BookChecks = Enumerable.Repeat(true, books.Count()).ToList()
            };

            return View("ClusteringResults", viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> LoadData(int loadDataFrom)
        {
            var dbooks = _bookService.GetAll();

            await _bookService.DeleteAll();

            var rootPath = _hostingEnvironment.WebRootPath;
            var filesPath = rootPath + "\\books\\testData";

            switch (loadDataFrom)
            {
                case 1:
                    filesPath += "\\small";
                    break;
                case 2:
                    filesPath += "\\medium";
                    break;
                case 3:
                    filesPath += "\\large";
                    break;
                case 4:
                    filesPath += "\\ultra";
                    break;
                default:
                    break;
            }

            var files = Directory.GetFiles(filesPath);

            var userId = _userManager.GetUserId(User);
            if (userId == null)
            {
                ViewBag.ErrorMessage = $"Пользователь с Id : {userId} не может быть найден";
                return NotFound();
            }

            var user = _userManager.FindByIdAsync(userId).Result;

            foreach (var file in files)
            {
                var text = System.IO.File.ReadAllText(file, System.Text.Encoding.GetEncoding(1251));
                var transformedText = TransformText(text);

                var filename = file.Replace(filesPath + "\\", "").Split(new char[] { '-', '.'});
                string author = "", title = "", genres = "";
                int year = 0;
                try
                {
                    author = filename[0];
                    title = filename[1];
                    genres = filename[2];
                    year = int.Parse(filename[3]);
                }
                catch (Exception)
                {
                }

                var newBook = new Book
                {
                    Title = title,
                    Author = author,
                    Genre = genres,
                    Year = year,
                    ImageUrl = "/images/books/noimage.png",
                    PublicationDate = DateTime.Now,
                    Status = "Avaliable",
                    ISBN = GetIsbn(),
                    User = user,
                    Content = text,
                    ProcessedText = string.Join(",", transformedText.Item1.ToArray()),
                    PortedText = string.Join(",", transformedText.Item2.ToArray()),
                    Rating = 0
                };

                await _bookService.Add(newBook);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> DeleteAll()
        {
            var dbooks = _bookService.GetAll();

            await _bookService.DeleteAll();
            return RedirectToAction("Index");
        }

        private (List<string>, List<string>) TransformText(string text)
        {
            var processedText = new List<string>();
            var porterText = new List<string>();

            foreach (var term in _regex.Split(text))
            {
                var temp1 = Regex.Replace(term.ToLower(), @"[^а-я]", string.Empty);

                var temp2 = Porter.TransformWord(temp1);

                if (temp1 != "")
                {
                    processedText.Add(temp1);
                    porterText.Add(temp2);
                }
            }

            return (processedText, porterText);
        }

        public string GetIsbn()
        {
            var random = new Random();
            string str = string.Empty;
            for (int i = 0; i < 10; i++)
                str = string.Concat(str, random.Next(10).ToString());
            return str;
        }
    }
}
