﻿using Microsoft.AspNetCore.Mvc;

namespace MyLib.Controllers.ActionResults
{
    public class JavaScriptResult : ContentResult
    {
        public JavaScriptResult(string script)
        {
            Content = script;
            ContentType = "application/javascript";
        }
    }
}
