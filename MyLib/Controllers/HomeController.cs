﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyLib.Services.Interfaces;
using MyLib.ViewModels.Book;
using MyLib.ViewModels.Tag;

namespace MyLib.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IBook _bookService;
        private readonly IRating _ratingService;

        public HomeController(ILogger<HomeController> logger, IBook bookService, IRating ratingService)
        {
            _logger = logger;
            _bookService = bookService;
            _ratingService = ratingService;
        }

        public IActionResult Index()
        {
            var books = _bookService.GetAll().OrderByDescending(x => x.Rating).ToList();

            var booksOnPage = books
                .Select(book => new BookViewModel
                {
                    BookId = book.Id,
                    Title = book.Title,
                    ImageUrl = book.ImageUrl,
                    Author = book.Author,
                    Genre = book.Genre,
                    Rating = book.Rating,
                    PublicationDate = book.PublicationDate,
                    UserId = book.User.Id,
                    Year = book.Year,
                    Description = book.Description,
                    Tags = book.BookTags.Where(bt => bt.BookId == book.Id).ToList().Select(bt => new TagViewModel
                                                                                    {
                                                                                        Id = bt.Tag.Id,
                                                                                        Name = bt.Tag.Name,
                                                                                        Type = bt.Tag.Type
                                                                                    })
                }).Take(5).ToList();

            List<int> rates = new List<int>();
            foreach (var book in booksOnPage)
                rates.Add(_ratingService.GetAll().Where(x => x.Book.Id == book.BookId).ToList().Count);

            ViewBag.Rates = rates;

            return View(booksOnPage);
        }
    }
}
