﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyLib.Data.Models;
using MyLib.Services.Interfaces;
using MyLib.ViewModels.UserProfile;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace MyLib.Controllers
{
    public class UserController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IUser _userService;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IBook _bookService;
        private readonly IForum _forumService;
        private readonly IComment _commentService;
        private readonly SignInManager<User> _signInManager;
        private readonly IRating _ratingService;

        public UserController(IUser userService,
            UserManager<User> userManager,
            IWebHostEnvironment hostingEnvironment, 
            IBook bookService, IForum forumService, IComment commentService,
            SignInManager<User> signInManager,
            IRating ratingService)
        {
            _userService = userService;
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
            _bookService = bookService;
            _forumService = forumService;
            _commentService = commentService;
            _signInManager = signInManager;
            _ratingService = ratingService;
        }

        /*public async Task<IActionResult> Detail(string id)
        {
            var user = await _userManager.FindByIdAsync(id);

            if (user == null)
            {
                ViewBag.ErrorMassage = $"Не найден пользователь с Id = {id}";
                return NotFound();
            }

            var userRoles = await _userManager.GetRolesAsync(user);

            var model = new UserProfileViewModel
            {
                UserId = user.Id,
                UserName = user.UserName,
                FullName = user.FullName,
                Age = user.Age,
                Email = user.Email,
                ExistingImageUrl = user.ProfileImageUrl,
                Phone = user.PhoneNumber,
                UserRating = user.Rating,
                UserRoles = userRoles,
                IsAdmin = userRoles.Contains("Admin")
            };

            return View(model);
        }*/

        [HttpPost] //Заменил ГЕТ на ПОСТ, чтоб уж наверняка никто в будущем не хакнул никого)))
        public async Task<IActionResult> Detail(UserProfileViewModel model)
        {
            if (_userManager.GetUserId(User) == model.UserId)
            {
                var user = await _userManager.FindByIdAsync(model.UserId);

                /*if (user == null)
                {
                    ViewBag.ErrorMassage = $"Не найден пользователь с Id = {id}";
                    return NotFound();
                }*/

                var userRoles = await _userManager.GetRolesAsync(user);

                var model2 = new UserProfileViewModel
                {
                    UserId = user.Id,
                    UserName = user.UserName,
                    FullName = user.FullName,
                    Age = user.Age,
                    Email = user.Email,
                    ExistingImageUrl = user.ProfileImageUrl,
                    Phone = user.PhoneNumber,
                    UserRating = user.Rating,
                    UserRoles = userRoles,
                    IsAdmin = userRoles.Contains("Admin")
                };

                return View(model2);
            }
            else return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> EditProfile(UserProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(model.UserId);

                if (user == null)
                {
                    ViewBag.ErrorMassage = $"Не найден пользователь с Id = {model.UserId}";
                    return NotFound();
                }
                else
                {
                    user.FullName = model.FullName;
                    user.Email = model.Email;
                    user.Age = model.Age;
                    user.PhoneNumber = model.Phone;
                    user.ProfileImageUrl = UploadProfileImage(model) ?? user.ProfileImageUrl;
                }
                
                await _userService.Edit(user);

                return RedirectToAction("Index", "Home");
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteProfile(string id)
        {
            if (id == null)
            {
                ViewBag.ErrorMassage = $"Не найден пользователь с Id = {id}";
                return NotFound();
            }

            var user = await _userManager.FindByIdAsync(id);
            if (!user.IsDeleted)
                user.IsDeleted = true;

            await _userManager.UpdateAsync(user);

            await _signInManager.SignOutAsync(); // иначе исключение

            return RedirectToAction("Index", "Home");
        }

        private string UploadProfileImage(UserProfileViewModel model)
        {
            if (model.Image != null)
            {
                var uploadsFolder = _hostingEnvironment.WebRootPath + "\\images\\profile";
                var fileUrl = model.Image.FileName;
                var filePath = Path.Combine(uploadsFolder, fileUrl);
                var stream = new FileStream(filePath, FileMode.Create);
                model.Image.CopyTo(stream);
                stream.Close();

                return @"/images/profile/" + fileUrl;
            }

            return null;
        }

        public async Task<IActionResult> Profile(string id)
        {
            User user = await _userManager.FindByIdAsync(id);
            if (user.IsDeleted)
                return RedirectToAction("Index", "Home");
            else
            {
                var books = _bookService.GetAll().ToList();
                ViewBag.BooksQuantity = books.Where(x => x.User.Id == id).ToList().Count;
                var posts = _forumService.GetAll().ToList();
                ViewBag.PostsQuantity = posts.Where(x => x.User.Id == id).ToList().Count;
                var comments = _commentService.GetAll().ToList();
                ViewBag.CommentsQuantity = comments.Where(x => x.User.Id == id).ToList().Count;

                var bestThreeBooks = books.Where(x => x.User.Id == id).OrderByDescending(x => x.Rating).ToList().Take(3);
                ViewBag.BestThreeBooks = bestThreeBooks.ToList();

                List<int> rates = new List<int>();
                foreach (var book in bestThreeBooks)
                    rates.Add(_ratingService.GetAll().Where(x => x.Book.Id == book.Id).ToList().Count);

                ViewBag.Rates = rates;

                if (user == null)
                {
                    ViewBag.ErrorMassage = $"Не найден пользователь с Id = {id}";
                    return NotFound();
                }

                var userRoles = await _userManager.GetRolesAsync(user);

                var model = new UserProfileViewModel
                {
                    UserId = user.Id,
                    UserName = user.UserName,
                    FullName = user.FullName,
                    Age = user.Age,
                    Email = user.Email,
                    ExistingImageUrl = user.ProfileImageUrl,
                    Phone = user.PhoneNumber,
                    UserRating = user.Rating,
                    UserRoles = userRoles,
                    IsAdmin = userRoles.Contains("Admin")
                };

                return View(model);
            }

        }
    }
}
