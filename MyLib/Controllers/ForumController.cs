﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyLib.Data.Models;
using MyLib.Services.Interfaces;
using MyLib.ViewModels;
using MyLib.ViewModels.Comment;
using MyLib.ViewModels.Forum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLib.Controllers
{
    public class ForumController : Controller
    {
        private readonly IForum _forumService;
        private readonly IComment _commentService;
        private readonly UserManager<User> _userManager;
        private readonly IReport _reportService;

        public ForumController(IForum forumService, 
            IComment commentService,
            UserManager<User> userManager, IReport reportService)
        {
            _forumService = forumService;
            _commentService = commentService;
            _userManager = userManager;
            _reportService = reportService;
        }

        [HttpPost]
        public IActionResult Search(string searchQuery)
        {
            return RedirectToAction("Index", new { searchQuery });
        }

        public IActionResult Index(string searchQuery, int page = 1)
        {
            int pageSize = 3;
            var posts = _forumService.GetFilteredPosts(searchQuery).ToList();
            var noResults = !string.IsNullOrEmpty(searchQuery) && !posts.Any();

            List<List<CommentViewModel>> comments = new List<List<CommentViewModel>>();
            foreach (var post in posts)
                comments.Add(BuildComments(post.Comments).ToList());
            ViewBag.Comments = comments;

            var postsListing = posts
                .Select(post => new PostViewModel
                {
                    PostId = post.Id,
                    Title = post.Title,
                    Created = post.Created,
                    Content = post.Content,
                    LikeCount = post.LikeCount,
                    DislikeCount = post.DislikeCount,
                    AuthorId = post.User.Id,
                    AuthorName = post.User.UserName,
                    AuthorRaiting = post.User.Rating,
                    AuthorImageUrl = post.User.ProfileImageUrl,
                    LastUpd = post.LastUpd,
                    Comments = BuildComments(post.Comments).OrderByDescending(x => x.Created)
                }).OrderByDescending(f => f.LastUpd).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            PageViewModel pageViewModel = new PageViewModel(posts.Count(), page, pageSize, searchQuery);

            var forum = new ForumDetailModel
            {
                NumberOfPosts = postsListing?.Count() ?? 0
            };

            var model = new ForumIndexModel
            {
                Forum = forum,
                SearchQuery = searchQuery,
                EmptySearchResult = noResults,
                PageViewModel = pageViewModel,
                Posts = postsListing
            };

            return View(model);
        }

        public IActionResult Detail(int id)
        {
            var post = _forumService.GetById(id);
            var comments = BuildComments(post.Comments);

            var model = new PostViewModel
            {
                PostId = post.Id,
                Title = post.Title,
                AuthorId = post.User.Id,
                AuthorName = post.User.UserName,
                AuthorImageUrl = post.User.ProfileImageUrl,
                AuthorRaiting = post.User.Rating,
                Created = post.Created,
                Content = post.Content,
                LikeCount = post.LikeCount,
                DislikeCount = post.DislikeCount,
                Comments = comments,
                LastUpd = post.LastUpd
            };

            return View(model);
        }

        private bool IsAuthorAdmin(User user)
        {
            return _userManager.GetRolesAsync(user).Result.Contains("Admin");
        }

        /*[HttpPost]
        public async Task<IActionResult> LikePost(int id)
        {
            var post = _forumService.GetById(id);
            if (post != null)
            {
                post.LikeCount += 1;
                await _forumService.Update(post);
            }
            else
            {
                ViewBag.ErrorMassage = $"Не найден пост с Id = {post.Id}";
                return NotFound();
            }

            return RedirectToAction("Index", "Forum");
        }


        [HttpPost]
        public async Task<IActionResult> DislikePost(int id)
        {
            var post = _forumService.GetById(id);
            if (post != null)
            {
                post.DislikeCount += 1;
                await _forumService.Update(post);
            }
            else
            {
                ViewBag.ErrorMassage = $"Не найден пост с Id = {post.Id}";
                return NotFound();
            }

            return RedirectToAction("Index", "Forum");
        }*/

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost, ActionName("Create")]
        public async Task<IActionResult> AddPost(PostViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = _userManager.GetUserId(User);
                if (userId == null)
                {
                    ViewBag.ErrorMessage = $"Пользователь с Id : {userId} не может быть найден";
                    return NotFound();
                }

                var user = _userManager.FindByIdAsync(userId).Result;

                var post = new Forum
                {
                    Id = model.PostId,
                    Title = model.Title,
                    Content = model.Content,
                    Created = DateTime.Now,
                    User = user,
                    LastUpd = DateTime.Now
                };

                await _forumService.Create(post);
                //add soon
                //await _userService.UpdateUserRating(userId, typeof(Post));

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            await _forumService.Delete(id);
            return RedirectToAction("Index");
        }

        public IActionResult Update(int id)
        {
            var post = _forumService.GetById(id);

            var forumUpdateView = new PostViewModel
            {
                PostId = post.Id,
                Title = post.Title,
                Content = post.Content,
                Created = post.Created
            };

            return View(forumUpdateView);
        }

        [HttpPost, ActionName("Update")]
        public async Task<IActionResult> EditPost(PostViewModel model)
        {
            if (ModelState.IsValid)
            {
                var post = _forumService.GetById(model.PostId);
                post.Title = model.Title;
                post.Content = model.Content;
                post.Created = model.Created;
                post.LastUpd = DateTime.Now;

                await _forumService.Update(post);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddComment(PostViewModel model)
        {
            if (model.CommentContent != null)
            {
                var userId = _userManager.GetUserId(User);
                var user = await _userManager.FindByIdAsync(userId);

                var comment = BuildReply(model, user);
                await _commentService.Create(comment);

                var post = _forumService.GetById(model.PostId);
                post.LastUpd = comment.Created;
                await _forumService.Update(post);

                //add soon
                //await _forumService.UpdateUserRating(userId, typeof(PostReply));

                return RedirectToAction("Detail", new { id = model.PostId });
            }

            return RedirectToAction("Detail", new { id = model.PostId });
        }

        [HttpPost]
        [Route("Forum/EditComment/{commentId}")]
        public async Task<IActionResult> EditComment(PostViewModel model, int commentId)
        {
            if (model != null)
            {
                var comment = _commentService.GetById(commentId);
                comment.Content = model.CommentContent;

                await _commentService.Update(comment);

                return RedirectToAction("Detail", new { id = model.PostId });
            }

            return RedirectToAction("Detail", new { id = model.PostId });
        }

        [HttpPost]
        [Route("Forum/DeleteComment/{commentId}")]
        public async Task<IActionResult> DeleteComment(int commentId, int postId)
        {
            await _commentService.Delete(commentId);
            return RedirectToAction("Detail", new { id = postId });
        }

        [HttpPost]
        [Route("Forum/SendReport/{commentId}")]
        public async Task<IActionResult> SendReport(PostViewModel model, int commentId)
        {
            if (model != null)
            {
                var comment = _commentService.GetById(commentId);
                var userId = _userManager.GetUserId(User);
                var user = await _userManager.FindByIdAsync(userId);

                await _reportService.Add(new Report
                {
                    Reason = model.ReportReason,
                    Description = model.ReportDescription,
                    ReportDate = DateTime.Now,
                    IsRead = false,
                    Comment = comment,
                    UserReporter = user
                });

                return RedirectToAction("Detail", new { id = model.PostId });
            }
            return RedirectToAction("Detail", new { id = model.PostId });
        }


        private IEnumerable<CommentViewModel> BuildComments(IEnumerable<Comment> comments)
        {
            return comments.Select(comment => new CommentViewModel
            {
                CommentId = comment.Id,
                Created = comment.Created,
                Content = comment.Content,
                AuthorId = comment.User.Id,
                AuthorName = comment.User.UserName,
                AuthorImageUrl = comment.User.ProfileImageUrl,
                AuthorRating = comment.User.Rating,
                PostId = comment.Forum.Id,
                IsAuthorAdmin = IsAuthorAdmin(comment.User)
            });
        }

        private Comment BuildReply(PostViewModel model, User user)
        {
            var currentPost = _forumService.GetById(model.PostId);

            return new Comment
            {
                Content = model.CommentContent,
                Created = DateTime.Now,
                Forum = currentPost,
                User = user
            };
        }
    }
}
