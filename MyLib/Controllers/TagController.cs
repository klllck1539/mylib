﻿using Microsoft.AspNetCore.Mvc;
using MyLib.Data.Models;
using MyLib.Services.Interfaces;
using MyLib.ViewModels.Tag;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLib.Controllers
{
    public class TagController : Controller
    {
        private readonly ITag _tagService;
        private readonly IBook _bookService;

        public TagController(ITag tagService, IBook bookService)
        {
            _tagService = tagService;
            _bookService = bookService;
        }

        public IActionResult Index()
        {
            var tags = _tagService.GetAll();
            return View(tags);
        }

        public IActionResult CreateTag()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateTag(TagViewModel model)
        {
            if (ModelState.IsValid)
            {
                var newTag = new Tag
                {
                   Name = model.Name,
                   Type = model.Type,
                };

                await _tagService.Add(newTag);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpPost]
        [Route("Tag/DeleteTag/{tagId}")]
        public async Task<IActionResult> DeleteTag(int tagId)
        {
            var tag = _tagService.GetById(tagId);
            if (tag == null)
            {
                ViewBag.ErrorMassage = $"Не найден тег с Id = {tagId}";
                return NotFound();
            }
            else
            {
                await _tagService.Delete(tag.Id);

                return RedirectToAction("Index");
            }
        }

        [Route("Tag/EditTag/{tagId}")]
        public IActionResult EditTag(int tagId)
        {
            var tag = _tagService.GetById(tagId);
            if (tag == null)
            {
                ViewBag.ErrorMassage = $"Не найден тег с Id = {tagId}";
                return NotFound();
            }

            List<BookTagsViewModel> booksWithTagViewModel = new List<BookTagsViewModel>();

            foreach (var book in _bookService.GetAll())
            {
                if (book.BookTags.Select(bt => bt.Tag.Id == tag.Id).Contains(true))
                {
                    booksWithTagViewModel.Add(new BookTagsViewModel
                    {
                        BookId = book.Id,
                        BookTitle = book.Title,
                        BookImageUrl = book.ImageUrl
                    });
                }
            }

            var model = new TagViewModel
            {
                Id = tag.Id,
                Name = tag.Name,
                Type = tag.Type,
                Books = booksWithTagViewModel
            };

            return View(model);
        }

        [HttpPost]
        [Route("Tag/EditTag/{Id}")]
        public async Task<IActionResult> EditTag(TagViewModel model)
        {
            var tag = _tagService.GetById(model.Id);
            if (tag == null)
            {
                ViewBag.ErrorMassage = $"Не найден тег с Id = {model.Id}";
                return NotFound();
            }
            else
            {
                tag.Name = model.Name;
                tag.Type = model.Type;

                await _tagService.Edit(tag);

                return RedirectToAction("Index");
            }
        }

        [Route("Tag/AddTagsToBook/{tagId}")]
        public IActionResult AddTagsToBook(int tagId)
        {
            var tag = _tagService.GetById(tagId);
            if (tag == null)
            {
                ViewBag.ErrorMassage = $"Не найден тег с Id = {tagId}";
                return NotFound();
            }

            ViewBag.tagName = tag.Name;
            ViewBag.tagId = tagId;

            var model = new List<BookTagsViewModel>();
            var books = _bookService.GetAll();

            foreach (var book in books)
            {
                var bookTagsModel = new BookTagsViewModel
                {
                    BookId = book.Id,
                    BookTitle = book.Title,
                    BookImageUrl = book.ImageUrl,
                    BookAuthor = book.Author
                };

                if (book.BookTags.Any(bt => bt.Tag.Id == tag.Id))
                {
                    bookTagsModel.IsSelected = true;
                }
                else
                {
                    bookTagsModel.IsSelected = false;
                }

                model.Add(bookTagsModel);
            }

            return View(model);
        }

        [HttpPost]
        [Route("Tag/AddTagsToBook/{tagId}")]
        public async Task<IActionResult> AddTagsToBook(List<BookTagsViewModel> model, int tagId)
        {
            var tag = _tagService.GetById(tagId);
            if (tag == null)
            {
                ViewBag.ErrorMassage = $"Не найден тег с Id = {tagId}";
                return NotFound();
            }

            for (int i = 0; i < model.Count; i++)
            {
                var book = _bookService.GetById(model[i].BookId);

                if (model[i].IsSelected)
                {
                    await _tagService.AddTagToBook(book, tag);

                }
                else if (!model[i].IsSelected)
                {
                    await _tagService.RemoveTagFromBook(book, tag);
                }
                else
                {
                    continue;
                }

                if (i < model.Count - 1)
                    continue;
                else
                    return RedirectToAction("EditTag", new { Id = tagId });
            }

            return RedirectToAction("EditTag", new { Id = tagId });
        }
    }
}
