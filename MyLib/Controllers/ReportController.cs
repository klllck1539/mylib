﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyLib.Data.Models;
using MyLib.Services.Interfaces;
using MyLib.ViewModels;
using MyLib.ViewModels.Report;
using System.Linq;
using System.Threading.Tasks;

namespace MyLib.Controllers
{
    public class ReportController : Controller
    {
        private readonly IReport _reportService;
        private readonly UserManager<User> _userManager;

        public ReportController(UserManager<User> userManager, IReport reportService)
        {
            _reportService = reportService;
            _userManager = userManager;
        }
        public ActionResult Index(int page = 1)
        {
            //if (User.IsInRole("MainAdmin"))
            //{
                int pageSize = 10;
                var reports = _reportService.GetAll().Where(x => !x.IsRead).ToList();

                var reportsOnPage = reports.Select(rep => new ReportViewModel
                {
                    ReportId = rep.Id,
                    ReportReason = rep.Reason,
                    ReportDescription = rep.Description,
                    ReportDate = rep.ReportDate,
                    CommentId = rep.Comment.Id,
                    CommentCreated = rep.Comment.Created,
                    CommentContent = rep.Comment.Content,
                    CommentAuthorId = rep.Comment.User.Id,//нулль
                    CommentAuthorFullName = rep.Comment.User.FullName,//нулль
                    UserReporterId = rep.UserReporter.Id,
                    UserReporterFullName = rep.UserReporter.FullName
                }).Skip((page - 1) * pageSize).Take(pageSize).ToList();

                PageViewModel pageViewModel = new PageViewModel(reports.Count(), page, pageSize, null);

                var model = new ReportIndexModel
                {
                    PageViewModel = pageViewModel,
                    Reports = reportsOnPage
                };
                return View(model);
            //}
           //else return RedirectToAction("Account", "AccessDenied");
        }

        [HttpPost]
        public async Task <IActionResult> CheckReport(int id)
        {
            //if (User.IsInRole("MainAdmin"))
            //{
            var rep =  _reportService.GetById(id);
            await _reportService.CheckReport(rep);
                
            return RedirectToAction("Index");
            //}
            //else return RedirectToAction("Account", "AccessDenied");
        }

        //[HttpPost]
        public ActionResult GetCountReports()
        {
            var reports = _reportService.CountUnread();
            return Json(reports);
        }
    }
}