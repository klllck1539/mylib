﻿using System;

namespace MyLib.ViewModels
{
    public class PageViewModel
    {
        public int PageNumber { get; set; }
        public int ChapterNumber { get; set; }
        public int TotalPages { get; set; }
        public string SearchQuery { get; set; }

        public PageViewModel(int count, int pageNumber, int pageSize, string searchQuery)
        {
            SearchQuery = searchQuery;
            PageNumber = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            ChapterNumber = 1;
        }

        //For Read Book
        public PageViewModel(int count, int chapterNumber, int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            ChapterNumber = chapterNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
        }

        public bool HasPreviousPage
        {
            get
            {
                return PageNumber > 1;
            }
        }

        public bool HasNextPage
        {
            get
            {
                return PageNumber < TotalPages;
            }
        }
    }
}
