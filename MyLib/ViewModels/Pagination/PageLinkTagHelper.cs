﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace MyLib.ViewModels.Pagination
{
    public class PageLinkTagHelper : TagHelper
    {
        private IUrlHelperFactory _urlHelperFactory;

        public PageLinkTagHelper(IUrlHelperFactory helperFactory)
        {
            _urlHelperFactory = helperFactory;
        }

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }
        public PageViewModel PageModel { get; set; }
        public string PageAction { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            IUrlHelper urlHelper = _urlHelperFactory.GetUrlHelper(ViewContext);
            output.TagName = "div";

            int ch = PageModel.ChapterNumber;
            string search = PageModel.SearchQuery;

            TagBuilder tag = new TagBuilder("ul");
            tag.AddCssClass("pagination");

            if (PageModel.TotalPages <= 5)
            {
                for (int page = 1; page <= PageModel.TotalPages; page++)
                {
                    TagBuilder currentItem = CreateTag(search, page, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(currentItem);
                }
            }
            else
            {
                //если текущая == 1:
                if (PageModel.PageNumber <= 3)
                {
                    TagBuilder firstItem = CreateTag(search, 1, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(firstItem);

                    TagBuilder secondItem = CreateTag(search, 2, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(secondItem);

                    TagBuilder thirdItem = CreateTag(search, 3, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(thirdItem);

                    if (PageModel.PageNumber == 3)
                    {
                        TagBuilder fourthItem = CreateTag(search, 4, ch, urlHelper);
                        tag.InnerHtml.AppendHtml(fourthItem);
                    }

                    TagBuilder dots = CreateTag(search, 0, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(dots);

                    TagBuilder lastItem = CreateTag(search, PageModel.TotalPages, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(lastItem);
                }
                else if (PageModel.TotalPages - PageModel.PageNumber <= 2)
                {
                    TagBuilder beginItem = CreateTag(search, 1, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(beginItem);

                    TagBuilder dots = CreateTag(search, 0, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(dots);

                    if (PageModel.TotalPages - PageModel.PageNumber == 2)
                    {
                        TagBuilder fourthItem = CreateTag(search, PageModel.PageNumber - 1, ch, urlHelper);
                        tag.InnerHtml.AppendHtml(fourthItem);
                    }

                    TagBuilder thirdItem = CreateTag(search, PageModel.TotalPages - 2, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(thirdItem);

                    TagBuilder secondItem = CreateTag(search, PageModel.TotalPages - 1, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(secondItem);

                    TagBuilder firstItem = CreateTag(search, PageModel.TotalPages, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(firstItem);
                }
                else
                {
                    TagBuilder firstItem = CreateTag(search, 1, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(firstItem);

                    TagBuilder dots = CreateTag(search, 0, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(dots);

                    TagBuilder prevItem = CreateTag(search, PageModel.PageNumber - 1, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(prevItem);

                    TagBuilder currentItem = CreateTag(search, PageModel.PageNumber, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(currentItem);

                    TagBuilder nextItem = CreateTag(search, PageModel.PageNumber + 1, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(nextItem);

                    tag.InnerHtml.AppendHtml(dots);

                    TagBuilder lastItem = CreateTag(search, PageModel.TotalPages, ch, urlHelper);
                    tag.InnerHtml.AppendHtml(lastItem);
                }
            }
            output.Content.AppendHtml(tag);
        }

        TagBuilder CreateTag(string searchQuery, int pageNumber, int chapterNumber, IUrlHelper urlHelper)
        {
            TagBuilder item = new TagBuilder("li");
            TagBuilder link = new TagBuilder("a");

            if (string.IsNullOrEmpty(searchQuery))
            {
                if (pageNumber == PageModel.PageNumber)
                {
                    item.AddCssClass("active");
                }
                else
                {
                    link.Attributes["href"] = urlHelper.Action(PageAction, new { page = pageNumber, chapter = chapterNumber });
                }
            }
            else
            {
                if (pageNumber == PageModel.PageNumber)
                {
                    item.AddCssClass("active");
                }
                else
                {
                    link.Attributes["href"] = urlHelper.Action(PageAction, new { searchQuery = searchQuery, page = pageNumber, chapter = chapterNumber });
                }
            }
            
            item.AddCssClass("page-item");
            if (pageNumber == 0)
                item.AddCssClass("disabled");
            link.AddCssClass("page-link");
            if (pageNumber != 0)
                link.InnerHtml.Append(pageNumber.ToString());
            else link.InnerHtml.Append("...");
            item.InnerHtml.AppendHtml(link);
            return item;

        }
    }
}
