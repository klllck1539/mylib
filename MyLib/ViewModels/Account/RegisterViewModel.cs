﻿using System.ComponentModel.DataAnnotations;

namespace MyLib.ViewModels.Account
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Не указан Email")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Некорректный адрес")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        //[RegularExpression(@"(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{6,}", ErrorMessage = "Проверьте правильность пароля")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string PasswordConfirm { get; set; }

        [Required(ErrorMessage = "Укажите ваше полное имя")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Укажите ваш возраст")]
        [Range(1, 100)]
        public int Age { get; set; }

        [Required(ErrorMessage = "Укажите ваш номер телефона")]
        public string Phone { get; set; }

        //[Required]
        //public string ProfileImageUrl { get; set; }
    }
}
