﻿using k_clustering.k_means.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLib.ViewModels.Clustering
{
    public class ClusteringResultsViewModel
    {
        public List<List<ClusteringBookViewModel>> BookClusters { get; set; }
        public List<int> ClusterIds { get; set; }
        public List<int> BookIds { get; set; }
        public List<bool> BookChecks { get; set; }
        public List<int> BookChecksInt { get; set; }
        public List<int> TagIds { get; set; }
        public List<int> TagChecksInt { get; set; }
        public List<ClusteringTagViewModel> Tags { get; set; }
    }
}
