﻿using MyLib.Data.Models;
using System.Collections.Generic;

namespace MyLib.ViewModels.Clustering
{
    public class ClusteringBookViewModel
    {
        public int BookId { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public string Content { get; set; }
        public string ProcessedText { get; set; }
        public string PortedText { get; set; }
        public IEnumerable<BookTag> Tags { get; set; }
    }
}
