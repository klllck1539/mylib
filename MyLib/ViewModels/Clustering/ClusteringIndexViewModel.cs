﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLib.ViewModels.Clustering
{
    public class ClusteringIndexViewModel
    {
        public int K { get; set; }
        public bool XMeans { get; set; }
        public bool MLNet { get; set; }
        public List<int> BookIds { get; set; }
        public List<bool> BookChecks { get; set; }
        public List<int> BookChecksInt { get; set; }
        public List<ClusteringBookViewModel> Books { get; set; }
        public List<ClusteringTagViewModel> Tags { get; set; }
        public string SearchQueryName { get; set; }
        public string SearchQueryTag { get; set; }
        public bool IsChecked { get; set; }
        public int kMeans { get; set; }

        public  int LoadDataFrom { get; set; }
    }
}
