﻿using MyLib.Data.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyLib.ViewModels.Clustering
{
    public class ClusteringTagViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public TagType Type { get; set; }
    }
}
