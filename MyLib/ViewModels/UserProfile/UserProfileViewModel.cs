﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace MyLib.ViewModels.UserProfile
{
    public class UserProfileViewModel
    {
        public string UserId { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public int Age { get; set; }
        public string Phone { get; set; }
        public double UserRating { get; set; }

        public IList<string> UserRoles { get; set; }
        public bool IsAdmin { get; set; }
        public DateTime MemberScince { get; set; }
        public string ProfileImageUrl { get; set; }
        public string ExistingImageUrl { get; set; }
        public IFormFile Image { get; set; }
    }
}
