﻿namespace MyLib.ViewModels.Tag
{
    public class BookTagsViewModel
    {
        public int BookId { get; set; }
        public string BookTitle { get; set; }
        public string BookImageUrl { get; set; }
        public bool IsSelected { get; set; }
        public string BookAuthor { get; set; }
    }
}
