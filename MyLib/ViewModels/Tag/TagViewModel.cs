﻿using MyLib.Data.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyLib.ViewModels.Tag
{
    public class TagViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Введите название для нового тега")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Укажите тип тега")]
        public TagType Type { get; set; }

        public List<BookTagsViewModel> Books { get; set; }
    }
}
