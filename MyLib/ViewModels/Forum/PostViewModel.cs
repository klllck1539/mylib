﻿using MyLib.ViewModels.Comment;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyLib.ViewModels.Forum
{
    public class PostViewModel
    {
        //PostData
        public int PostId { get; set; }

        [Required(ErrorMessage = "Ведите название темы")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Напишите что-нибудь по данной теме")]
        public string Content { get; set; }
        public DateTime Created { get; set; }

        public int LikeCount { get; set; }
        public int DislikeCount { get; set; }
        public bool IsLiked { get; set; }

        public DateTime LastUpd { get; set; }

        //AuthorsData
        public string AuthorId { get; set; }
        public string AuthorName { get; set; }
        public double AuthorRaiting { get; set; }
        public string AuthorImageUrl { get; set; }
        public bool IsAuthorAdmin { get; set; }

        //CommentsData
        public IEnumerable<CommentViewModel> Comments { get; set; }
        public string CommentContent { get; set; }
        public DateTime CommentCreated { get; set; }

        //Reports' data
        public int ReportId { get; set; }
        public int ReportReason { get; set; }
        public string ReportDescription { get; set; }
        public DateTime ReportDate { get; set; }
        public bool ReportIsRead { get; set; } //уведомление админу/модеру

        //FKs UserReporter
        public string UserReporterId { get; set; }
        public string UserReporterFullName { get; set; }
    }
}
