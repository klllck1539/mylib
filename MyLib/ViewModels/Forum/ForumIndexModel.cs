﻿using MyLib.ViewModels.Comment;
using System.Collections.Generic;

namespace MyLib.ViewModels.Forum
{
    public class ForumIndexModel
    {
        public IEnumerable<PostViewModel> Posts { get; set; }
        public IEnumerable<CommentViewModel> Comments { get; set; }
        public ForumDetailModel Forum { get; set; }
        public PageViewModel PageViewModel { get; set; }
        public string SearchQuery { get; set; }
        public bool EmptySearchResult { get; set; }

        public bool IsLiked { get; set; }
    }
}
