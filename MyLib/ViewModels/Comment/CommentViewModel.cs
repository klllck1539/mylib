﻿using MyLib.ViewModels.Forum;
using System;

namespace MyLib.ViewModels.Comment
{
    public class CommentViewModel
    {
        public int CommentId { get; set; }
        public string Title { get; set; }
        public DateTime Created { get; set; }
        public string Content { get; set; }

        public string AuthorId { get; set; }
        public string AuthorName { get; set; }
        public string AuthorImageUrl { get; set; }
        public double AuthorRating { get; set; }
        public bool IsAuthorAdmin { get; set; }

        public int PostId { get; set; }
        public string PostTitle { get; set; }
    }
}
