﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyLib.ViewModels.Administration
{
    public class RoleViewModel
    {
        public string RoleId { get; set; }

        [Required(ErrorMessage = "Введите название для новой роли")]
        public string RoleName { get; set; }

        public List<string> Users { get; set; }

        public RoleViewModel()
        {
            Users = new List<string>();
        }
    }
}
