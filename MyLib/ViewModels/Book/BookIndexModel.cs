﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLib.ViewModels.Book
{
    public class BookIndexModel
    {
        public string SearchQuery { get; set; }
        public PageViewModel PageViewModel { get; set; }
        public IEnumerable<BookViewModel> Books { get; set; }
    }
}
