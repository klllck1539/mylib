﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using MyLib.ViewModels.Tag;
using System;

namespace MyLib.ViewModels.Book
{
    public class BookViewModel
    {
        public int BookId { get; set; }

        [Required(ErrorMessage = "Введите название книги")]
        [StringLength(50, ErrorMessage = "Длина названия книги не должна превышать 50 символов!")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Введите название автора книги")]
        [StringLength(30, ErrorMessage = "Длина имени автора книги не должна превышать 30 символов!")]
        public string Author { get; set; }

        [Required(ErrorMessage = "Введите год издания книги")]
        [Range(1, 2020, ErrorMessage = "Недопустимое значение. Введите точный год издания книги")]
        public int Year { get; set; }

        [Required(ErrorMessage = "Укажите жанр книги")]
        public string Genre { get; set; }

        public string Content { get; set; }
        public string Description { get; set; }
        public DateTime PublicationDate { get; set; }
        public string Isbn { get; set; }
        public string Status { get; set; }
        public string ImageUrl { get; set; }
        public string ExistingImageUrl { get; set; }
        public IFormFile Image { get; set; }
        public IFormFile FileContent { get; set; }

        public double Rating { get; set; }
        public int RateValue { get; set; }

        public string UserId { get; set; }
        public IEnumerable<TagViewModel> Tags { get; set; }

        public PageViewModel Pages { get; set; }
        public List<string> Sentences { get; set; }
    }
}
