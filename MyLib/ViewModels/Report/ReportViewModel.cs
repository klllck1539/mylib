﻿using System;

namespace MyLib.ViewModels.Report
{
    public class ReportViewModel
    {
        public int ReportId { get; set; }

        //Atributes
        public int ReportReason { get; set; }
        public string ReportDescription { get; set; }
        public DateTime ReportDate { get; set; }
        public bool ReportIsRead { get; set; } //уведомление админу/модеру

        //FKs Comment
        public int CommentId { get; set; }
        public DateTime CommentCreated { get; set; }
        public string CommentContent { get; set; }

        public string CommentAuthorId { get; set; }
        public string CommentAuthorFullName { get; set; }

        public int PostId { get; set; }
        public string PostTitle { get; set; }
        //FKs UserReporter
        public string UserReporterId { get; set; }
        public string UserReporterFullName { get; set; }
    }
}
