﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLib.ViewModels.Report
{
    public class ReportIndexModel
    {
        public PageViewModel PageViewModel { get; set; }
        public IEnumerable<ReportViewModel> Reports { get; set; }
    }
}
