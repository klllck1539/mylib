﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyLib.Clustering.TextClustering.MLObjects
{
    public class MLClusterPrediction
    {
        public int Id { get; set; }
        public uint PredictedLabel;
    }
}
