﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyLib.Clustering.TextClustering.MLObjects
{
    public class MLBook
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
}
