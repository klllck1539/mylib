﻿using k_clustering.k_means.Objects;
using Microsoft.ML;
using MyLib.Clustering.TextClustering.MLObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyLib.Clustering.TextClustering
{
    public class MLClustering
    {
        MLContext mlContext = new MLContext();

        public List<Cluster> Cluster(IEnumerable<MLBook> mlBooks, int k)
        {
            var dataView = mlContext.Data.LoadFromEnumerable(mlBooks);
            var transformedData = mlContext.Transforms.Text.FeaturizeText("Features", "Text").Fit(dataView).Transform(dataView);

            var pipeline = mlContext.Clustering.Trainers.KMeans(numberOfClusters: k);

            var model = pipeline.Fit(transformedData);

            var clusterPredictions = mlContext.Data.CreateEnumerable<MLClusterPrediction>(model.Transform(transformedData), false).ToList();
            clusterPredictions.Sort((x, y) => x.PredictedLabel.CompareTo(y.PredictedLabel));

            var clusters = new List<Cluster>();
            for (int i = 0; i < k; i++)
            {
                clusters.Add(new Cluster());
                clusters.Last().DocumentGroup = new List<DocumentVector>();
                foreach (var prediction in clusterPredictions)
                {
                    if (prediction.PredictedLabel == i + 1)
                    {
                        clusters.Last().DocumentGroup.Add(new DocumentVector { Id = prediction.Id });
                    }
                }
            }

            return clusters;
        }
    }
}
